��    (      \  5   �      p     q     y     �     �     �     �     �     �     �     �  |   �     f     m     �     �     �     �  	   �     �     �     �     �     �                         /     B     I     Q     ]  	   b  	   l     v     �  �   �     O     T    c     �  "   �     �     �     �  -   �          *     :     G  �   Z  
   *	  7   5	  7   m	  
   �	     �	     �	     �	     �	     �	  !   
  "   -
     P
     j
     y
     �
  -   �
  +   �
  
   �
     �
       
   !     ,     ;     X     u    �  
   �     �              %         "      &             
                               !            $       	           '                                    (                                 #    Account Account Chart Template Account Type All All Entries All Posted Entries Assets Auto Unfold Balance Balance Sheet Binary fields can not be exported to Excel unless their content is base64-encoded. That does not seem to be the case for %s. Cancel Chart of Account Hierarchy Chart of Accounts Hierarchy Code Credit Date From : Date To : Date(s) Debit Display Accounts Display Accounts : Display Name Equity Error Expense Hierarchy Based On : Hierarchy based on Income Journal Liabilities Name PRINT PDF PRINT XLS Profit & Loss Templates for Accounts The 'Internal Type' is used for features available on different types of accounts: liquidity type is for cash or bank accounts, payable/receivable is for vendor/customer accounts. Type With movements Project-Id-Version: Odoo Server 13.0+e-20200214
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-03-12 00:44+0200
Last-Translator: 
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: 
Language: ar
X-Generator: Poedit 2.3
 الحساب قالب دليل الحسابات نوع الحساب الكل كل عناصر اليومية كل عناصر اليومية المرحلة الأصول غير مطوي الرصيد الميزانية لا يمكن تصدير الحقول الثنائية لملف الإكسل إلا إذا كان محتواها مشفر بطريقة base64. يبدو أن الحقل %s لا يتبع هذه القاعدة. الغاء دليل الحسابات بالتسلسل الهرمي دليل الحسابات بالتسلسل الهرمي الكود الدائن تاريخ البداية تاريخ النهاية تواريخ المدين الحسابات المعروضة الحسابات المعروضة: الاسم المعروض الملكية خطأ المصروف التسلسل الهرمي بناء علي:  التسلسل الهرمي بناء علي الدخل دفتر اليومية الإلتزامات الاسم طباعة pdf تصدير الي اكسيل الربح والخسارة. قوالب الحسابات يُستخدم 'النوع الداخلي' للخصائص المتاحة على الأنواع المختلفة للحسابات: النوع الجاري للحسابات النقدية أو البنكية, والدائنة أو المدينة لحسابات المورد/العميل. النوع به قيود مرحلة فقط 