# -*- coding: utf-8 -*-

from odoo import models, fields, api


class Task(models.Model):
    _inherit='project.task'

    @api.model
    def create(self, values):
        res=super(Task, self).create(values)
        if values.get('user_id'):
            self.create_activity2(res.id,res.user_id,res.name)
        return res

    def write(self, values):
        res=super(Task, self).write(values)
        if values.get('user_id'):
            self.create_activity2(self.id,self.user_id,self.name)
        return res





    def create_activity2(self,res_id,user_id,task_name):
        activity_to_do = self.env.ref('mail.mail_activity_data_todo').id
        model_id = self.env['ir.model']._get('project.task').id
        activity = self.env['mail.activity']
        act_dct = {
            'activity_type_id': activity_to_do,
            'note':"This Task "+str(task_name)+" Assigned To You",
            'user_id': user_id.id,
            'res_id': res_id,
            'res_model_id': model_id,
        }

        activity.sudo().create(act_dct)









class User(models.Model):
    _inherit='res.users'
    parent_user_id_id = fields.Many2one(comodel_name="res.users",related='employee_id.parent_id.user_id',store=True)
    parent_user_id_id_3 = fields.Many2one(comodel_name="res.users",related='employee_id.department_id.parent_id.manager_id.user_id',store=True)
